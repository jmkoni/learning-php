<?php # Script 9.4 - view_users.php #4

// Retrieves all the records from the users table
// This new version links to edit and delete pages.

$page_title = 'View the Current Users';
include('includes/header.html');
echo '<h1>Registered Users</h1>';

require_once('includes/mysqli_connect.php');

// Number of records to show per page:
$display = 10;

// Determine how many pages there are...
if (isset($_GET['p']) && is_numeric($_GET['p'])) {
	$pages = $_GET['p'];
} else {
	// Count the number of records:
	$q = "select count(user_id) from users";
	$r = @mysqli_query ($dbc, $q);
	$row = @mysqli_fetch_array ($r, MYSQLI_NUM);
	$records = $row[0];
	
	// Calculate number of pages
	if ($records > $display) {
		$pages = ceil ($records/$display);
	} else {
		$pages = 1;
	}
}

// Determine where in the database to start returning results
if (isset($_GET['s']) && is_numeric($_GET['s'])) {
	$start = $_GET['s'];
} else {
	$start = 0;
}

// Make the query
$q = "select last_name, first_name, date_format(registration_date, '%M %d, %Y') as dr, user_id from users order by registration_date asc limit $start, $display";
$r = @mysqli_query ($dbc, $q);

// Table header:
echo '<table align="center" cellspacing="0" cellpadding="5" width="75%">
<tr>
<td align="left"><b>Edit</b></td>
<td align="left"><b>Delete</b></td>
<td align="left"><b>Last Name</b></td>
<td align="left"><b>First Name</b></td>
<td align="left"><b>Date Registered</b></td>
</tr>';

// Fetch and print all the records
$bg = '#eeeeee'; // Set the initial background color
while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
	$bg = ($bg=='#bfd4f0' ? '#dfebff' : '#bfd4f0'); // Switch the background color
	
	echo '<tr bgcolor="' . $bg . '">
	<td align="left"><a href="edit_user.php?id=' . $row['user_id'] . '">Edit</a></td>
	<td align="left"><a href="delete_user.php?id=' . $row['user_id'] . '">Delete</a></td>
	<td align="left">' . $row['last_name'] . '</td>
	<td align="left">' . $row['first_name'] . '</td>
	<td align="left">' . $row['dr'] . '</td>
	</tr>';
}

echo '</table>';
mysqli_free_result ($r);
mysqli_close($dbc);

// Make the links to other pages, if necessary
if ($pages > 1) {
	// Add some spacing and start a paragraph:
	echo '<br /><p>';
	
	// Determine what page the script is on:
	$current_page = ($start/$display) + 1;
	
	//If it's not eh first page, make a previous button:
	if ($current_page != 1) {
		echo '<a href="view_users.php?s=' . ($start - $display) . '&p=' . $pages . '">Previous</a> ';
	}
	
	// Make all the numbered pages:
	for ($i = 1; $i <= $pages; $i++) {
		if ($i != $current_page) {
			echo '<a href="view_users.php?s=' . (($display * ($i -1))) . '&p=' . $pages . '">' . $i . '</a> ';
		} else {
			echo $i . ' ';
		}
	}
	
	// If it's not the last page, make a next button
	if ($current_page != $pages) {
		echo '<a href="view_users.php?s=' . ($start + $display) . '&p=' . $pages . '">Next</a>';
	}
	
	echo '</p>';
}
include('includes/footer.html');
?>