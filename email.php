<?php # Script 12.1 - email.php #2

// Retrieves all the records from the users table
// This new version links to edit and delete pages.

$page_title = 'Contact Me';
include('includes/header.html');
echo '<h1>Contact Me</h1>';

// Set default timezone
date_default_timezone_set('America/New_York');

if (isset($_POST['submitted'])) {
	/* function takes one argument
	* function returns a clean version of the string
	* clean version may be either an empty string or just the removal of all newline characters
	*/
	function spam_scrubber($value) {
		// List bad values
		$very_bad = array('to:', 'cc:', 'bcc:', 'content-type:', 'mime-version:', 'multipart-mixed:', 'content-transfer-encoding:');
		
		// If any of the bad strings are in the submitted value, return an empty string:
		foreach ($very_bad as $v) {
			if (stripos($value, $v) !== false) {
				return '';
			}
		}
		
		// Replace any newline characters with spaces:
		$value = str_replace(array("\r", "\n", "%0a", "%0d"), ' ', $value);
		
		// Return the value:
		return trim($value);
	} // End of function
	
	// Clean the form data
	$scrubbed = array_map('spam_scrubber', $_POST);
	
	// form validation
	if (!empty($scrubbed['name']) && !empty($scrubbed['email']) && !empty($scrubbed['comments'])) {
		// create the body
		$body = "Name:
		{$scrubbed['name']}\n\nComments:
		{$scrubbed['comments']}";
		
		$body = wordwrap($body, 70);
		
		// Send the email:
		mail('jmorton429@gmail.com', 'Contact Form Submission', $body, "From: {$scrubbed['email']}");
		
		// Print a message:
		echo '<p><em>Thank you for contacting me at ' . date('g:i a(T)') . ' on ' . date('l F j, Y') . '. I will reply some day.</em></p>';
		
		// Clear $_POST
		$_POST = array();
		
	} else {
		echo '<p style="font-weight: bold; color: #C00">Please fill out the form completely.</p>';
	}
}
?>

<p>Please fill out this form to contact me.</p>

<form action="email.php" method="post">
	<p>Name: <input type="text" name="name" size="30" maxlength="60" value="<?php if(isset($_POST['name'])) echo $_POST['name']; ?>" /></p>
	<p>Email Address: <input type="text" name="email" size="30" maxlength="80" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>" /></p>
	<p>Comments: <textarea name="comments" rows="5" cols="30"><?php if(isset($_POST['comments'])) echo $_POST['comments']; ?></textarea></p>
	<p><input type="image" name="submit" src="http://mundu.com/im/images/send-button.png" value="Send!" /></p>
	<input type="hidden" name="start" value="<?php echo time(); ?>"/>
	<input type="hidden" name="submitted" value="TRUE" />
</form>

<?php
include('includes/footer.html');
?>