<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta name="description" content="learning php site" />
    <meta name="keywords" content="php, education, noob" />
    <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
	<title>Images</title>
	<script language="JavaScript">
	<!-- // Hide from old browsers.
	
	// Make a pop-up window function:
	function create_window (image, width, height) {
	
		// Add some pixels to the width and height:
		width = width + 10;
		height = height + 10;
		
		// If the window is already open, 
		// resize it to the new dimensions:
		if (window.popup && !window.popup.closed) {
			window.popup.resizeTo(width, height);
		} 
		
		// Set the window properties:
		var specs = "location=no, scrollbars=no, menubars=no, toolbars=no, resizable=yes, left=0, top=0, width=" + width + ", height=" + height;
		
		// Set the URL:
		var url = "show_image.php?image=" + image;
		
		// Create the pop-up window:
		popup = window.open(url, "ImageWindow", specs);
		popup.focus();
		
	} // End of function.
	//--></script>
	<link rel="stylesheet" href="includes/style.css" type="text/css" media="screen" />
</head>
<body>
  <div id="main">
  	<div id="login">
	  	<?php // Create a login/logout link:
	  	echo "<p>Hi";
		if ((isset($_COOKIE['user_id'])) && (!strpos($_SERVER['PHP_SELF'],'logout.php')) ) {
		  echo " {$_COOKIE['first_name']}! <a href=\"logout.php\">Logout</a>";
		} else {
		  echo '! <a href="login.php">Login</a> or <a href="register.php">Register</a>';
		}
		echo "</p>"
		?>
	</div>
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.html">Jennifer<span class="logo_colour">Morton</span></a></h1>
          <h2>Learnin' PHP one script at a time...</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
			<li><a href="view_users.php">View Users</a></li>
			<li><a href="password.php">Change Password</a></li>
			<li><a href="email.php">Contact Me</a></li>
			<li><a href="images.php">Images</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
      <div id="sidebar_container">
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <!-- insert your sidebar items here -->
            <h3>Latest News</h3>
            <h4>New Website Launched</h4>
            <h5>January 1st, 2010</h5>
            <p>2010 sees the redesign of our website. Take a look around and let us know what you think.<br /><a href="#">Read more</a></p>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Useful Links</h3>
            <ul>
              <li><a href="#">link 1</a></li>
              <li><a href="#">link 2</a></li>
              <li><a href="#">link 3</a></li>
              <li><a href="#">link 4</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Search</h3>
            <form method="post" action="#" id="search_form">
              <p>
                <input class="search" type="text" name="search_field" value="Enter keywords....." />
                <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="includes/search.png" alt="Search" title="Search" />
              </p>
            </form>
          </div>
          <div class="sidebar_base"></div>
        </div>
      </div>
      <div id="content">
      <p>Click on an image to view it in a separate window.</p>
<table align="center" cellspacing="5" cellpadding="5" border="1">
	<tr>
		<td align="center"><b>Image Name</b></td>
		<td align="center"><b>Image Size</b></td>
	</tr>
<?php # Script 10.4 - images.php
// This script lists the images in the uploads directory.

$dir = '../uploads'; // Define the directory to view.

$files = scandir($dir); // Read all the images into an array.

// Display each image caption as a link to the JavaScript function:
foreach ($files as $image) {

	if (substr($image, 0, 1) != '.') { // Ignore anything starting with a period.
	
		// Get the image's size in pixels:
		$image_size = getimagesize ("$dir/$image");
		
		// Calculate the image's size in kilobytes:
		$file_size = round ( (filesize ("$dir/$image")) / 1024) . "kb";
		
		// Make the image's name URL-safe:
		$image = urlencode($image);
		
		// Print the information:
		echo "\t<tr>
		\t\t<td><a href=\"javascript:create_window('$image',$image_size[0],$image_size[1])\">$image</a></td>
		\t\t<td>$file_size</td>
		\t</tr>\n";
	
	} // End of the IF.
    
} // End of the foreach loop.
?>
</table>
<a href="upload_image.php">Upload Image</a>
<?php
include('includes/footer.html');
?>
