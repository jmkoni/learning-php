<?php # Script 9.2 - delete_user.php

// This page is for deleting a user record.

$page_title = 'Delete a User';
include ('includes/header.html');
echo '<h1>Delete a User</h1>';

// Check for a valid user ID, through GET or POST:
if ((isset($_GET['id'])) && (is_numeric($_GET['id']))) {
	$id = $_GET['id'];
} elseif ((isset($_POST['id'])) && (is_numeric($_POST['id']))) {
	$id = $_POST['id'];
} else {
	echo '<p class="error">This page has been accessed in error.</p>';
	include ('includes/footer.html');
	exit();
}

require_once('includes/mysqli_connect.php');

// Check if the form has been submitted:
if (isset($_POST['submitted'])) {
	if ($_POST['sure'] == 'Yes') {
		// Make the query:
		$q = "delete from users where user_id=$id limit 1";
		$r = @mysqli_query ($dbc, $q);
		if (mysqli_affected_rows($dbc) == 1) {
			echo '<p>The user has been deleted.</p>';
		} else {
			echo '<p class="error">The user could not be deleted due to a system error.</p>';
			echo '<p>' . mysqli_error($dbc) . '<br />Query: ' . $q . '</p>'; // Debugging message
		}
	} else {
		echo '<p>The user has NOT been deleted.</p>';
	}
} else {
	// Retrieve the users information:
	$q = "select concat(last_name, ', ', first_name) from users where user_id=$id";
	$r = @mysqli_query ($dbc, $q);
	
	if (mysqli_num_rows($r) == 1) {
		// Get the user's information
		$row = mysqli_fetch_array ($r, MYSQLI_NUM);
		
		// Create the form
		echo '<form action="delete_user.php" method="post">
		<h3>Name: ' . $row[0] . '</h3>
		
		<p>Are you sure you want to delete this user?<br />
		
		<input type="radio" name="sure" value="Yes" /> Yes
		<input type="radio" name="sure" value="No" checked="checked" /> No</p>
		
		<p><input type="submit" name="submit" value="Submit" /></p>
		
		<input type="hidden" name="submitted" value="TRUE" />
		
		<input type="hidden" name="id" value="' . $id . '" />
		</form>';
	} else {
		echo '<p class="error">This page has been accessed in error.</p>';
	}
}

mysqli_close($dbc);
include('includes/footer.html');
?>