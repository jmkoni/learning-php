<?php # Script 9.1 - view_users.php #3

// Retrieves all the records from the users table
// This new version links to edit and delete pages.

$page_title = 'View the Current Users';
include('includes/header.html');

echo '<h1>Registered Users</h1>';

// Connect to the database
require_once('includes/mysqli_connect.php');

// Make the query
$q = "select last_name, first_name, date_format(registration_date, '%M %d, %Y') as dr, user_id from users order by registration_date asc";
$r = @mysqli_query ($dbc, $q); // Run query

$num = mysqli_num_rows($r);

if ($num > 0) { // If it ran, display the records
	echo "<p>There are currently $num registered users.</p>\n";
	echo '<table align="center" cellspacing="3" cellpadding="3" width="75%">
	<tr>
	<td align="left"><b>Edit</b></td>
	<td align="left"><b>Delete</b></td>
	<td align="left"><b>Last Name</b></td>
	<td align="left"><b>First Name</b></td>
	<td align="left"><b>Date Registered</b></td>
	</tr>';
	
	while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
		echo '<tr>
		<td align="left"><a href="edit_user.php?id=' . $row['user_id'] . '">Edit</a></td>
		<td align="left"><a href="delete_user.php?id=' . $row['user_id'] . '">Delete</a></td>
		<td align="left">' . $row['last_name'] . '</td>
		<td align="left">' . $row['first_name'] . '</td>
		<td align="left">' . $row['dr'] . '</td>
		</tr>';
	}
	
	echo '</table>';
	
	mysqli_free_result ($r); // Free up resources
} else {
	echo '<p class="error">The current users could not be retrieved. Oops!</p>';
	// Debugging message
	echo '<p>' . mysqli_error($dbc) . '<br /><br />Query: ' . $q . '</p>';
}

mysqli_close($dbc);

include('includes/footer.html');
?>