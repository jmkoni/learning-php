<?php # Script 13.3 - replace.php

$page_title = 'Testing PCRE Replace';

include ('includes/header.html');
include ('includes/report_errors.php');

if (isset($_POST['submitted'])) {
	
	// Trim the strings
	$pattern = trim($_POST['pattern']);
	$subject = trim($_POST['subject']);
	$replace = trim($_POST['replace']);
	
	// Print a caption:
	echo "<p>The result of replacing<br /><b>$pattern</b><br />with<br />$replace<br />in<br />$subject<br />is ";
	// regex to test an email: /^[\w.-]+@[\w.-]+\.[A-Za-z]{2,6}\r?$/m
	
	// Test:
	if (preg_match($pattern, $subject)) {
		echo preg_replace($pattern, $replace, $subject) . '</p>';
	} else {
		echo 'Pattern was not found!</p>';
	}
}
?>
<form action="replace.php" method="post">
	<p>Regular Expression Pattern: <input type="text" name="pattern" value="<?php if (isset($pattern)) echo $pattern; ?>" size="30" /></p>
	<p>Replacement: <input type="text" name="replace" value="<?php if (isset($replace)) echo $replace; ?>" size="30" /></p>
	<p>Test Subject: <textarea name="subject" rows="5" cols="30" value="<?php if (isset($subject)) echo $subject; ?>" size="30" ></textarea></p>
	<input type="submit" name="submit" value="Test!" />
	<input type="hidden" name="submitted" value="TRUE" />
</form>
<?php
include ('includes/footer.html');
?>