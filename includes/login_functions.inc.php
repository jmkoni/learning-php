<?php # Script 11.2 - login_functions.inc.php

// This page defines two functions used by the login/logout process.

/* This function determines and returns an absolute URL.
   * It takes one argument: the page that concludes the URL.
   * The argument defaults to index.php.
*/

function absolute_url ($page = 'index.php') {
	// Start defining the URL
	// URL is http:// plus the host name plus the current directory:
	$url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
	
	// Remove any trailing slashes:
	$url = rtrim($url, '/\\');
	
	// Add the page:
	$url .= '/' . $page;
	
	// Return the URL:
	return $url;
}

/* This function validates the form data
   * If both are present, the database is queried.
   * The function requires a database connection
   * The function returns an array of information, including:
     - a boolean value indicating success
     - an array of either errors or the database result
*/
function check_login($dbc, $email='', $pass='') {
	$errors = array();  // Initialize the array
	
	// Validate the email address
	if (empty($email)) {
		$errors[] = 'You forgot to enter your email address.';
	} else {
		$e = mysqli_real_escape_string($dbc, trim($email));
	}
	
	// Validate the password
	if (empty($pass)) {
		$errors[] = 'You forgot to enter your password.';
	} else {
		$p = mysqli_real_escape_string($dbc, trim($pass));
	}
	
	// Everything's alright
	if (empty($errors)) {
		// Retrieve the user_id and first_name for that email/password combination:
		$q = "select user_id, first_name from users where email='$e' and pass=sha1('$p')";
		$r = @mysqli_query ($dbc, $q);  // Run the query
/*
		$q = "select user_id, first_name from users where email=? and pass=sha1(?)";
		$stmt = mysqli_prepare($dbc, $q);
		mysqli_stmt_bind_param($stmt, 'ss', $e, $p);
*/
		
		// Check the result
		if (mysqli_num_rows($r) == 1) {
			// Fetch the record
			$row = mysqli_fetch_array ($r, MYSQLI_ASSOC);
			
			// Return true and the record
			return array(true, $row);
		} else {
			// Not a match
			$errors[] = 'The email address and password entered do not match those on file.';
		}
	}
	// Return false and the errors
	return array(false, $errors);
}
?>