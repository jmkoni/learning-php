<?php # Script 11.11 - logout.php #2

// This page lets the user logout.
session_start(); // Access the existing session

//If no cookie is present, redirect the user:

if (!isset($_SESSION['user_id'])) {
	// Need the functions to create an absolute URL
	require_once('includes/login_functions.inc.php');
/* 	$url = absolute_url(); */
	$url = absolute_url($page = 'login.php');
	header("Location: $url");
	exit();
} else {
	// Cancel the session
	$_SESSION = array(); // Clear the variables
	session_destroy(); // Destroy the session
	setcookie('PHPSESSID', '', time()-3600, '/', '', 0, 0); // Destroy the cookie
}

// Set the page title and include the html header
$page_title = 'Logged Out!';
include('includes/header.html');

// Print a customized message
echo "<h1>Logged Out!</h1>
<p>You are now logged out!</p>";

include('includes/footer.html');
?>